# -*- mode: ruby -*-
# vi: set ft=ruby :

# All Vagrant configuration is done below. The "2" in Vagrant.configure
# configures the configuration version (we support older styles for
# backwards compatibility). Please don't change it unless you know what
# you're doing.
Vagrant.configure(2) do |config|
  # The most common configuration options are documented and commented below.
  # For a complete reference, please see the online documentation at
  # https://docs.vagrantup.com.

  # Every Vagrant development environment requires a box. You can search for
  # boxes at https://atlas.hashicorp.com/search.
  config.vm.box = "debian/jessie64"

  #config.ssh.username = "root"
  #config.ssh.password = "vagrant"


  # Disable automatic box update checking. If you disable this, then
  # boxes will only be checked for updates when the user runs
  # `vagrant box outdated`. This is not recommended.
  # config.vm.box_check_update = false

  # Create a forwarded port mapping which allows access to a specific port
  # within the machine from a port on the host machine. In the example below,
  # accessing "localhost:8080" will access port 80 on the guest machine.
  config.vm.network "forwarded_port", guest: 6543, host: 6543
  config.vm.network "forwarded_port", guest: 8080, host: 8080

  # Create a private network, which allows host-only access to the machine
  # using a specific IP.
  # config.vm.network "private_network", ip: "192.168.33.10"

  # Create a public network, which generally matched to bridged network.
  # Bridged networks make the machine appear as another physical device on
  # your network.
  # config.vm.network "public_network"

  # Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
  # config.vm.synced_folder "../data", "/vagrant_data"

  # Provider-specific configuration so you can fine-tune various
  # backing providers for Vagrant. These expose provider-specific options.
  # Example for VirtualBox:
  #
  # config.vm.provider "virtualbox" do |vb|
  #   # Display the VirtualBox GUI when booting the machine
  #   vb.gui = true
  #
  #   # Customize the amount of memory on the VM:
  #   vb.memory = "1024"
  # end
  config.vm.provider "virtualbox" do |v|
      v.memory = 2048
      v.cpus = 2
  end
  #
  # View the documentation for the provider you are using for more
  # information on available options.

  # Define a Vagrant Push strategy for pushing to Atlas. Other push strategies
  # such as FTP and Heroku are also available. See the documentation at
  # https://docs.vagrantup.com/v2/push/atlas.html for more information.
  # config.push.define "atlas" do |push|
  #   push.app = "YOUR_ATLAS_USERNAME/YOUR_APPLICATION_NAME"
  # end

  # Enable provisioning with a shell script. Additional provisioners such as
  # Puppet, Chef, Ansible, Salt, and Docker are also available. Please see the
  # documentation for more information about their specific syntax and use.
  config.vm.provision "shell", inline: <<-SHELL
    sudo apt-get update
    sudo apt-get install -y git python-setuptools apt-transport-https mongodb redis-server redis-tools
    sudo apt-get install -y python-dev libldap2-dev gcc libsasl2-dev
    sudo apt-get install -y openssl libpython-dev libffi-dev libssl-dev
    git clone https://osallou@bitbucket.org/osallou/go-docker.git
    sudo rm -f /home/vagrant/go-docker/plugins/mesos.*
    git clone https://osallou@bitbucket.org/osallou/go-docker-web.git
    git clone https://osallou@bitbucket.org/osallou/go-docker-vagrant.git
    sudo easy_install pip
    /usr/bin/yes | sudo pip uninstall six
    cd /home/vagrant/go-docker && sudo pip install -r requirements.txt
    cd /home/vagrant/go-docker && sudo python setup.py develop
    cd /home/vagrant/go-docker-web && sudo pip install -r requirements.txt
    cd /home/vagrant/go-docker-web && sudo python setup.py develop
    sudo pip install godocker_cli 
    sudo apt-key adv --keyserver hkp://pgp.mit.edu:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D
    sudo echo "deb https://apt.dockerproject.org/repo debian-jessie main" > /etc/apt/sources.list.d/docker.list
    sudo apt-get update
    sudo apt-get -y install docker-engine
    sudo docker pull centos:latest
    sudo docker pull rastasheep/ubuntu-sshd
    sudo mkdir -p /etc/systemd/system/docker.service.d
    #echo "DOCKER_OPTS=\"-H tcp://127.0.0.1:2375\"" | sudo tee --append /etc/default/docker
    echo "DOCKER_OPTS=\"-H tcp://127.0.0.1:2375\"" > /etc/systemd/system/docker.service.d/godocker.conf
    #sudo sed -i 's;ExecStart=/usr/bin/dockerd -H fd://;ExecStart=/usr/bin/docker daemon -H 127.0.0.1:2375;g' /lib/systemd/system/docker.service
    sudo systemctl daemon-reload
    sudo service docker restart
    sudo mkdir -p /home/vagrant/shared
    sudo chown -R vagrant /home/vagrant/shared
    sudo mkdir -p /opt/shared
    sudo chown -R vagrant /opt/shared
    sudo cp /home/vagrant/go-docker-vagrant/default_godocker /etc/default/godocker
    sudo mkdir /var/run/godocker
    sudo chown -R vagrant /var/run/godocker
    sudo cp /home/vagrant/go-docker-vagrant/*.service /lib/systemd/system/
    sudo cp /home/vagrant/go-docker-vagrant/go-d.ini /home/vagrant/go-docker/
    sudo cp /home/vagrant/go-docker-vagrant/production.ini /home/vagrant/go-docker-web/
    sudo systemctl enable  god-scheduler.service
    sudo systemctl enable  god-watcher.service
    sudo systemctl enable  god-web.service
    sudo service god-scheduler start
    sudo service god-watcher start
    sudo service god-web start
    python /home/vagrant/go-docker/seed/create_local_user.py -c /home/vagrant/go-docker/go-d.ini -l vagrant -p vagrant -h /home/vagrant --uid 1000 --gid 1000
   SHELL
end
